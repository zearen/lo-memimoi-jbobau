# lo memimoi jbobau

I.e., "the Lojban I speak".  As is modern tradition, this is a directory
specifying the features and reforms I use when speaking Lojban, even including
some reforms of my own split out into separate pages so they can be adopted
individually.

# jo'au la gadganzu

I use the gadganzu as articles.  These articles represent a range of meaning.
The [wiki article][gadganzu-wiki] explains it well, though I use a different
version of «voi».  I also use singular logic, to clarify.

I have a more detailed version of how I use articles in the yet incomplete
[gadgrafu][] article where I go into detail on a clarification how I believe
gadanzu quantifiers should interact.

I unify sets and masses, because masses are really just sets with the same
cardinality as the continuum.

# jo'au lodu lo'i cmevla cu brivla kei

I merge cmevla into brivla, as is common.  Then all cmevla have the same place
structure of:

.cmen.: x1 is a referent of the name "Cmen".

# jo'au lo sampu jonvla no'u la jacus

I use a modified form of the simplified connective system described
[here][sampu-jonvla].  I merge JA and JOI, and use them in all the afterthought
positions, including «jacu» and «japoi».  However, I still use GA as described
in the CLL, except that they are terminated with «gi'i» and GA and GU'A are
merged.  This seems to be the modern style.  Note that in situations like
«do ga bajra gi cadzu» where it's ambiguous whether it's bridi tail forethought
or tanru forethought connection, it defaults to bridi tail.
«do cu ga bajra gi cadzu» would be tanru forethought connection.

Also, I use «vu'o» as a general terminator for afterthought connection, so
it can be followed be anything that could follow any sumti, including UI.
Whatever follows then applies to all of the jorne and seljorne as a unit.

> mi jau do vu'o po'o cu klama

Roughly means "((me and you) only) go."

# jo'au lo srana lo namcu

I use some esoteric numeric features slightly differently.  This rarely comes up
in speech, but I use different digits for values greater than 12₁₀₁₀, and I
specify numeric bases differently.  The details can be found [here](namcu.md).

# jo'au la .ajbo

I use .ajbo, which means I recycle the «.V» cmavo freed from la jacus as
sentence starters of selma'o I.  «.i» is the single most common word in Lojban,
so it might be useful to add some variety.  .ajbo allows one to set the mood of
the following sentence, and each sentence link is similar to «.i» + some UI.

| .ajbo | .iUI    | Mood                                                    |
|-------|---------|---------------------------------------------------------|
| .i    | .i      | Default sentence link                                   |
| .a    | .ipau   | Interrogative mood.  The sentence contains a question.  |
| .e    | .iji'a  | Continuative mood.  Somewhat similar to a semicolon.    |
| .o    | .iko'oi | Imperative mood.  The sentence contains a command.      |
| .u    | .iku'i  | Contrastive mood.  Similar to "but" or "however".       |

«.anai» is sometimes used to signal a response.

# jo'au la jboponei

I rarely use it, but I make grammatical space for jboponei.  «po» is equivalent
to «lo su'u» and is terminated with «nei», which is swapped for gi'o which was
freed from use of la jacus.

# jo'au la tcekitau

I use many tcekitau swaps and steals.  Not every word needs a replacement,
in particular, as explained above, I use different digits for numbers over 12,
so all the remaining hex digits are free.  In the chart below, I indicate the
source as one of:

- Some reform that frees the cmavo in the *memimoi* column to be assigned the
  CLL or present definition of the word in the *CLL* column
- Steal, meaning there is no new equivalent for the CLL meaning of the word in
  the *memimoi* column and the words become synonyms for the meaning of the word
  in the *CLL* column.
- A new word, which is like "Steal", but provides a replacement for the
  otherwise lost meaning.
- Swap, the cmavo have their meanings swapped.
- Shift, this is a chain of meaning changes.  The word in the *memoimi* column
  Gets the meaning of the word in the *CLL* column, and its current meaning
  is freed in a later row.

| CLL   | memimoi | Source |
|-------|---------|--------|
| ce'u  | ce      | Steal  |
| ke'a  | ki      | Swap   |
| tu'a  | tau     | Steal  |
| jo'u  | jau     | Steal  |
| du'u  | du      | Swap   |
| ru'e  | rei     | Steal  |
| nei   | gi'o    | jacus  |
| fi'o  | foi     | lerfu  |
| zo'u  | zau     | Steal  |
| su'o  | su      | su'u'u |
| la'au | la'u    | Shift  |
| la'u  | la'i    | Steal  |


# jo'au lo lerfu

I use a reform to the [lerfu][] system.  For most cases, there's no noticeable 
difference, except I sometimes spell vowels «V'y».  «.y'y» is still &lt;'&gt;,
and &lt;y&gt; is «.iy'y».

# jo'au la cnino ke valsi smuni

I use the following redefinitions of gismu which more closely match Lojbanic
thought or have better aesthetics for buiding statements, i.e. which require
less grammatical machinery like «nu», «se», and «fe» in the common case.

- **dukse**: x1 is too much in property x2 ka to be/do x3 ka by standard x4
- **banzu**:  x1 is sufficiently x2 ka for purposes x3 (nu/pu'u/ka),
  under conditions x4
- **traji** %= veteve: x1 is the most x2 (ka) among set/mass x3 by ordering
  relation x4 (default zmadu)
- **simlu** %= setese: x1 seems to x2 to satisfy x3 (ka) under condition x4
- **cinse**: x1 exhibits sexual attraction to x2 in behavior/aspect (ka/nu) x3
  by standard x4
- **prane**: x1 is prefect at being x2 (ka) by standard x3
    - Simply add the x3 place
- **bandu**: x1 protects x2 (object/event) from threat/peril x3 (nu)
    - Then the old bandu is faurbandu
- **.irci**: x1 is a user of messaging service on channel x2 in network/server
  x3 using service x4 (IRC, Discord, telegram, matrix).
- **lerpoi** %= setese: p1 is a character string spelling x2=l3=p3 in character
  set x3=l2
- **mutce**: x1 is much/very x2 (ka+xokau) by standard x3
- **milxe**: x1 is mild/little in x2 (ka+xokau) by standard x3
    - The idea is that these two 
- **zekri**: x1 is a criminal for performing crime x2 (ka+nu), which is a crime
  against/by standard/as determined by (e.g. jury) x3.

I also use the following experimentals and synonyms:

- **skuna** = cusku
- **jbobo** = lojbo
    - without -loj- rafsi
- **dzama**: x1 does chore / menial duty / housework (not necessarily in house)
  / activity of daily living x2 for purpose / as obliged by x3
    - I steal the rafsi -dza- from «da» for this because it deserves it.  Some
      of the lujvo Ilmen defined with it are to be re-assigned to zi'evla -dava.
- **zgadi**: x1 should / ought be true under conditions x2 according to x3.
- **laldo** = tolni'o: x1 is old by standard x2
- **fegli** = tolmle: x1 is ugly to x2 by standard x3
- **tsuku** = darca = tolyli'a: x1 arrives at x2 via route x3
- **ronsa**: x1 is redundant/unnecessary with x2 because in property x3 (ka)
  under conditions / because of reason x4 (nu/du)
    - Steal the rafsi -ros- for this.  It currently belongs to prosa, but it has
      two, and there are no -ros- derived lujvo in la jbovlaste.

# jo'au lo cmavo be ma'oi bai poi smuni frica

I change the meaning of some modals:

- **ka'a**
  - Was the klama modal
  - Is now the kansa modal

[gadgrafu]: gadgrafu.md
[lerfu]: lerfu.md
[ajbo-wiki]: https://wiki.lojban.io/New_Sentence_Links
[gadganzu-wiki]: https://mw.lojban.org/papri/zipcpi:_Yet_another_gadri_article
[sampu-jonvla]: https://solpahi.wordpress.com/2016/09/20/a-simpler-connective-system/
