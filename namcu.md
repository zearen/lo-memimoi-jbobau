# jo'au la traji nacpau ju'u zei mekso je la me zo cy'au nacpau

This is "Highest Digit Base «ju'u»" and «cy'au» style digits.  Credit to la
srasu for the first idea, and la .ilmen. for the digits.  I propose the nickname
«jo'au la sraju'u» for this proposal.

«ju'u» is the VU"U, or operator, used to specify bases.  As defined in the CLL,
this specifies the base in the same way as in English, where base 10 is the
default base.  Then:

| English | CLL Lojban |
|---------|------------|
| base 2  | ju'u re    |
| base 6  | ju'u xa    |
| base 10 | ju'u pano  |
|         | ju'u dau   |
| base 16 | ju'u paxa  |

However, this doesn't make sense practically, because from the perspective of
any base, it is base always base 10.  Lojban has many digits, but not every one
is well known.  The other option is to specify a base as *the largest digit*
that base can use.  Then our base would be referred to as base 9.  I.e., the
same chart above is now:

| English | CLL Lojban |
|---------|------------|
| base 2  | ju'u pa    |
| base 6  | ju'u mu    |
| base 10 | ju'u so    |
| base 16 | ju'u my'ai |

You may note that base 16 is «my'ai», and not the CLL Lojban digit for 15 «vai».
This is because I use new numbers for the higher digits as suggested by Ilmen.
The digits over gai (12) have been reallocated for tcekitau steals.

| ## | CLL | memimoi |
|----|-----|---------|
| 10 | dau | dau     |
| 11 | fei | fei     |
| 12 | gai | gai     |
| 13 | jau | cy'au   |
| 14 | rei | vy'ei   |
|    | xei |         |
| 15 | vai | my'ai   |
| 16 |     | xy'au   |
| 17 |     | zy'ei   |
| 18 |     | by'ai   |
| 19 |     | sy'au   |

Note that this provides numerals for up to base 20, the highest base with
individual digits that is in common use (e.g. Mayan), though theoretically this
list could be extended to 42 since the suffixes are a three cycle, and digits
are a ten cycle, but this could become difficult when one has to do mental
number theory to remember if, e.g. «xy'ei» is 16, 26, or 36.

Regargless, for composite bases, one can use multiple «ju'u» specifying a
separator.  For example, base 60 could be expressed:

> paci ju'u pi'e muso ju'ubo so

Where the second base indicates the base use to specify the first.  «pi'e»
as a prefix indicates that we're using digit groupings of the specified maximum
number in each grouping.  Also note that I'm using «bo» instead of «bi'e»,
because there's no reason to have a separate cmavo for VU"U precedence.We can
also use this for specifying number format templates.  For example, the
traditional 24 hour clock as 24:60:60.000 could be specified as:

> repa pi'e cibi pi'e civo pi sozemu
> ju'u reci pi'e muso pi'e muso pi sososo
> ju'ubo so

Or you could just be cheeky and say:

> ju'u ni'e le se junla

It's also worth mentioning that if you're a fan of a partucular base (*ahem*
[base 6][jan6]), you can set it as the base for an entire passage with «ju'ai»
of selma'o MAI.  Note this reform solves the problem this word brings up in its
notes: Base six is just «muju'ai» in all contexts.

Finally, I note that ju'u does not have a rafsi, nore is the rafsi -ju'u-
assigned.  It might be really nice to be able to, say, define:

> **felju'u**: Digit string x1 (me'o PAboi...) is interpreted in base 12 as
> value (li) x2 from digit(s) x3 (list)

You can talk about the base itself using abstractions like anything else in
Lojban:

> mi nelci losi'o mumju'u

[jan6]: https://www.youtube.com/watch?v=qID2B4MK7Y0
