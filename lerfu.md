# jo'au la lerfu genti'i

I use a modified lerfu system.  This started as just wanting to steal «foi» for
«fi'o» but as I teased it free, I accidentally reformed the more esoteric
features.  Woopsie !

# lo karsna (Vowels)

I use the .V'y style vowels, though sometimes I slip and use old .Vbu style
ones.

| memimoi | CLL  |
|---------|------|
| .a'y    | .abu |
| .e'y    | .ebu |
| .i'y    | .ibu |
| .o'y    | .obu |
| .u'y    | .ubu |
| .iy'y   | .ybu |
| .y'y    | .y'y |

The last row is included for clarity.

# lo selyle'u (Alphabets) jau lo srana be la tcekitau

There are five, yes five, cv'v cmavo just uses to change which alphabet is being
used when spelling things.  Not only are these almost never used, their meaning
can be achieved using «zai», a CVV that's also begging to be teckitau'd, and
these privelege some writing systems over others.  The only reason to have a
shorter version is to insert common letters from other alphabets, for example
&lt;&alpha;&gt; as «ge'o .a'y lo'a», where the «lo'a» is needed to switch back.
However, in all these cases, it's easier and clearer to just use «.alfas. bu».

As such we should free all of them, then use some of them to back fill some
other CVV words we steal, namely «zai» whose meaning is re-assigned to «lo'a»,
«ge'o» is used to replace both «tei» and «foi», and «ru'o» is also used to
disable automatic diacritic combining, both of which are explained under la
lerjmina explained below. Also, under the reasoning of the above paragraph,
«lau», which marks the following BY as punctuation, is redundant.  Simply write,
e.g., «preti bu» for &lt;?&gt;.

The following chart shows which cmavo should be freed, the substituted phrase
with the same meaning, and the old meaning, and whether we it is claimed for
some new purpose:

| cmavo | basti         | Old meaning             | Claimed |
|-------|---------------|-------------------------|---------|
| lo'a  | lo'a latmo bu | Latin alphabet shift    | Claimed |
|       | lo'a lojbo bu | Lojban alphabet shift   |         |
| ge'o  | lo'a xelso bu | Greek alphabet shift    | Claimed |
| je'o  | lo'a xebro bu | Hebrew alphabet shift   | Freed   |
| jo'o  | lo'a xrabo bu | Arabic alphabet shift   | Freed   |
| ru'o  | lo'a rusko bu | Cyrillic alphabet shift | Claimed |

In addition, this would allow one to specify ZLM as «lo'a zbalermorna bu» or
«lo'a .zbal. bu» for short (or «lo'a zy.» for *very* short).  Also, I've added
«lai'a» (selma'o MAI) as a long scope alphabet specifier.  E.g.

> lo'a zbalermorna bu lai'i me'o .iy .u'y lerpoi lo prami

I.e., this changes the default alphabet for the rest of the text.

# la lerjmina (Combining letters)

As defined in the CLL, the «tei ... foi» brackets are used to spell compound
graphemes, e.g.  &lt;é&gt; would be spelled as «tei ebu .akut. bu foi».  This is
unnecessarily verbose.  Instead, I propose that diacritics automatically combine
with the letter they're following, so &lt;é&gt; becomes simply «.e'y .akut. bu».
This is matches how most languages spell diacritics, and is especially
convenient for highly compositional systems like Zbalermorna, Devanagari, or
Hangul.  Sometimes combining is undesired, in which case we reclaim «ru'o» as a
BY that prevents joining.  Then, «.e'y ru'o .grav. bu» is similar to «e◌̀».  (to
Though, I suggest calling the back tick something different like «.tik. bu» or
something 😉 toi)

It is tempting then to eliminate to just eliminate «tei ... foi» entirely, but
those behind zantufa point out that they're useful for combining the radicals of
logographic languages like Chinese Hanzi, e.g. "«tei remna bu sanli bu foi» has
can be used to spell 'zoizoi 位zoi bu'".  We still wish to free «tei» and «foi»,
so instead we can replace this construction with an (associative,
non-communiative) "combining" affix «ge'o», also stolen from the old alphabet
shifts, that combines two lerfu.  Then, "位" becomes «remna bu ge'o sanli bu».

# lo nu sepli fa lo lervla lo na'uvla

Zantufa suggest separating «PA» and «BY» so that you can no longer have strings
containing both.  I adopt this, and continue to use boi as a terminator for both
lerfu and namcu strings.  However, I note that «se'e», which allows you to
specify characters with numeric codes, now needs special grammar, and then the
grammar of lerfu strings will need to be updated with it, but this is not a big
issue.

```
lerpoi <- lervla*
lervla <- BY / valsi BU / SEhE PA*
valsi <- ZO . / .
```

Or something like that.

# fanmo

In conclusion, we're left with a more ergonomic lerfu system with no fewer
features, and we've freed six cmavo, including 4 highly valuable CVV form cmavo.
The below chart summarizes the freed and re-assigned cmavo:


| cmavo | CLL  | basti | Method of freeing.
|-------|------|-------|------------------------------------------------------|
| zai   |      | lo'a  | Tcekitau freed from removing alphabet shifts         |
| lau   |      |       | Just spell punctuation with no pre-marker «preti bu» |
| tau   | tu'a | tu'a  | Classic tcekitau                                     |
| tei   |      | ge'o  | Except as an infix: «.a'y ge'o .e'y» = &lt;æ&gt;     |
| foi   | fi'o |       | See above.  Terminator not needed.                   |
| lo'a  | zai  |       | Remove alphabet shift cmavo and use «lo'a»           |
| ge'o  |      |       | Remove alphabet shift cmavo and use «lo'a»           |
| je'o  |      |       | Remove alphabet shift cmavo and use «lo'a»           |
| jo'o  |      |       | Remove alphabet shift cmavo and use «lo'a»           |
| ru'o  | \*   |       | Remove alphabet shift cmavo and use «lo'a»           |

\* No CLL mapping, but has a new use.  See above.
